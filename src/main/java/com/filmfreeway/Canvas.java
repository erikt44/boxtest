package com.filmfreeway;

import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * Represents the drawable area as a matrix of characters
 */
public class Canvas
{
	private final char[][] matrix;

	public Canvas(int width, int height)
	{
		matrix = new char[height][width];
		Arrays.stream(matrix).forEach(ar -> Arrays.fill(ar, ' '));
	}


	/**
	 * Sets a "pixel" on our canvas to a char value
	 * Returns true on success, false on failure (outside bounds)
	 */

	public boolean set(int x, int y, char c)
	{
		if (x >= 0 && y >= 0 && y < matrix.length && x < matrix[y].length)
		{
			matrix[y][x] = c;
			return true;
		}

		return false;
	}

	/**
	 * Gives a text rendering of the entire matrix suitable for printing
	 */
	@Override
	public String toString()
	{
		return Arrays.stream(matrix).map(String::new).collect(Collectors.joining("\n"));
	}
}
