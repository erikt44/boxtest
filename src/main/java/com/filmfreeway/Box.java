package com.filmfreeway;

import java.util.ArrayList;
import java.util.List;

public class Box
{

	private static final int BORDER_THICKNESS = 1; //Only supporting a border thickness of 1 right now for our boxes

	private final List<Box> children;
	private final Layout layout;

	public Box()
	{
		this(Layout.HORIZONTAL);
	}

	public Box(Layout lay)
	{
		children = new ArrayList<>();
		layout = lay;
	}

	public boolean addChild(Box b)
	{
		return children.add(b);
	}

	//A box without children should have 1 x 1 of padding inside (modeled as 1 padding on left and top).
	// With children there should be 1 unit of padding on the left and right but no vertical padding
	public int getInternalHorizontalPadding()
	{
		return getInternalLeftPadding() + getInternalRightPadding();
	}

	public int getInternalVerticalPadding()
	{
		return getInternalTopPadding() + getInternalBottomPadding();
	}


	public int getInternalLeftPadding()
	{
		return 1;
	}

	public int getInternalRightPadding()
	{
		return children.isEmpty() ? 0 : 1;
	}


	public int getInternalTopPadding()
	{
		return children.isEmpty() ? 1 : 0;
	}

	public int getInternalBottomPadding()
	{
		return 0;
	}


	public int getContentWidth()
	{

		return layout.calculateContentWidth(children);
	}

	public int getContentHeight()
	{
		return layout.calculateContentHeight(children);
	}

	/**
	 * Calculates total width = width of all the content (children) + padding + border
	 */
	public int getWidth()
	{
		return getContentWidth() + getInternalHorizontalPadding() + 2 * BORDER_THICKNESS;
	}

	public int getHeight()
	{
		return getContentHeight() + getInternalVerticalPadding() + 2 * BORDER_THICKNESS;
	}


	/**
	 * Renders the box on the canvas with the upper left corner at x, y.
	 */
	public void render(int x, int y, Canvas c)
	{
		//Top of the box
		c.set(x, y, '+');

		int numDash = getWidth() - 2 * BORDER_THICKNESS;
		for (int l = 1; l <= numDash; l++)
		{
			c.set(x + l, y, '-');
		}

		c.set(x + numDash + 1, y, '+');

		//middle of the box
		int numPipe = getHeight() - 2 * BORDER_THICKNESS;

		for (int l = 1; l <= numPipe; l++)
		{
			c.set(x, y + l, '|');
			c.set(x + numDash + 1, y + l, '|');
		}

		//bottom of the box
		c.set(x, y + numPipe + 1, '+');

		for (int l = 1; l <= numDash; l++)
		{
			c.set(x + l, y + numPipe + 1, '-');
		}

		c.set(x + numDash + 1, y + numPipe + 1, '+');

		renderChildren(x, y, c);
	}

	/**
	 * Handles rendering of any children with the help of the layout
	 */

	public void renderChildren(int x, int y, Canvas c)
	{
		//start rendering children at upper left + border + padding
		int startX = x + BORDER_THICKNESS + getInternalLeftPadding();
		int startY = y + BORDER_THICKNESS + getInternalTopPadding();

		layout.renderChildren(children, c, startX, startY);
	}

}
