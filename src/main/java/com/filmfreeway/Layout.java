package com.filmfreeway;

import java.util.List;

public enum Layout
{
	HORIZONTAL()
			{
				@Override
				public int calculateContentWidth(List<Box> children)
				{
					//width includes blank space between children
					return children.stream().mapToInt(Box::getWidth).sum() + Math.max(0, children.size() - 1);
				}

				@Override
				public int calculateContentHeight(List<Box> children)
				{
					//height is equal to the tallest box
					return children.stream().mapToInt(Box::getHeight).max().orElse(0);
				}

				@Override
				public void renderChildren(List<Box> children, Canvas c, int startX, int startY)
				{
					for (Box currentChild : children)
					{
						currentChild.render(startX, startY, c);
						startX += currentChild.getWidth() + 1; //next horizontal child starts with a space between previous
					}
				}
			},
	VERTICAL()
			{
				@Override
				public int calculateContentWidth(List<Box> children)
				{
					//equal to the widest box
					return children.stream().mapToInt(Box::getWidth).max().orElse(0);
				}

				@Override
				public int calculateContentHeight(List<Box> children)
				{
					//equal to the sum of all boxes (no spacing between vertical boxes)
					return children.stream().mapToInt(Box::getHeight).sum();
				}

				@Override
				public void renderChildren(List<Box> children, Canvas c, int startX, int startY)
				{
					for (Box currentChild : children)
					{
						currentChild.render(startX, startY, c);
						startY += currentChild.getHeight();
					}
				}

			};

	public abstract int calculateContentWidth(List<Box> children);

	public abstract int calculateContentHeight(List<Box> children);

	public abstract void renderChildren(List<Box> children, Canvas c, int startX, int startY);
}
