package com.filmfreeway;

import com.google.gson.Gson;

import java.io.FileReader;


public class Main
{

	public static void main(String[] args) throws Exception
	{

		Gson gson = new Gson();


		for (String arg : args)
		{
			try (FileReader reader = new FileReader(arg))
			{
				System.out.println("************ Start Rendering: " + arg + " ************");

				Box b = gson.fromJson(reader, Box.class);
				Canvas c = new Canvas(b.getWidth(), b.getHeight());
				b.render(0, 0, c);

				System.out.println(c);
				System.out.println("************ End Rendering: " + arg + " ************");
				System.out.println();
			}
		}


	}
}
